#ifndef GLOBALS
#define GLOBALS
#include <stdbool.h>
#include <limits.h>

/*@ axiomatic thread_variables_properties {
      logic ℤ SIZE reads \nothing;
      axiom some: 0 < SIZE <= INT_MAX;
    }
*/


unsigned long small_cpumask_bits;
unsigned long this_cpu;

struct cpumask {
	bool *bits;
};

struct cpumask *should_we_balance_tmpmask;


struct sched_group {
  int flags;
};

struct sched_domain {
        struct sched_group *groups;
	int flags;
};

struct rq {
	unsigned int            nr_running;
        unsigned int            ttwu_pending;
};

enum cpu_idle_type {
        CPU_IDLE,
        CPU_NOT_IDLE,
        CPU_NEWLY_IDLE,
	CPU_MAX_IDLE_TYPES
};

struct lb_env {
        struct sched_domain     *sd;
        int                     dst_cpu;
        struct rq               *dst_rq;
        enum cpu_idle_type      idle;
        struct cpumask          *cpus;
};
#endif
