(* Read the output printed by frama-c. Check that verything is proved.  For
what is proved, print the maximum time, if available.  For what is not
proved, print what is missing. *)

let debug = ref false

let inner s start finish len =
  let s = String.sub s start (len - finish - start) in
  match String.split_on_char ':' s with
    [_;s] -> s
  | [s] -> s
  | _ -> failwith "bad time"

let get_time s =
  let len = String.length s in
  if len < 4
  then None
  else
    let last = String.get s (len - 1) in
    if last <> ')'
    then None
    else
      if String.get s (len-3) = 'm' && String.get s (len-2) = 's'
      then
	begin
	  let res = 0.001 *. (float_of_string (inner s 1 3 len)) in
	  (if !debug then Printf.eprintf "%s becomes %f\n" s res);
	  Some res
	end
      else if String.get s (len-2) = 's'
      then
	(match String.split_on_char '\'' (inner s 1 2 len) with
	  [a;b] ->
	    let mins = float_of_string a in
	    let secs = float_of_string b in
	    let res = (mins *. 60.) +. secs in
	    (if !debug then Printf.eprintf "%s becomes %f\n" s res);
	    Some res	    
	| [s'] ->
	    let res = float_of_string s' in
	    (if !debug then Printf.eprintf "%s becomes %f\n" s res);
	    Some res
	| _ -> failwith "bad second time")
      else
	begin
	  let butlast = String.get s (len-2) in
	  if '0' <= butlast && butlast <= '9'
	  then None
	  else failwith (Printf.sprintf "not sure what to do with %s" s)
	end

let collect i =
  let res = ref [] in
  let curfile = ref "" in
  let completed = ref 0 in
  let wanted = ref 0 in
  let times = ref [] in
  let goal = ref "" in
  let prove = ref "" in
  let etime = ref "" in
  let qed = ref 0 in
  let altergo = ref 0 in
  let cvc4 = ref 0 in
  let cvc5 = ref 0 in
  let z3 = ref 0 in
  let dump _ =
    if !curfile <> ""
    then
      begin
	let mx =
	  if !times = []
	  then (0.,"unknown")
	  else List.hd(List.rev(List.sort compare !times)) in
	res := (!curfile,!completed,!wanted,mx,!etime,!qed,!altergo,!cvc4,!cvc5,!z3)::!res;
	curfile := "";
	completed := 0;
	wanted := 0;
	times := [];
	goal := "";
	prove := "";
	etime := "";
	qed := 0;
	altergo := 0;
	cvc4 := 0;
	cvc5 := 0;
	z3 := 0;
      end in
  let rec loop _ =
    let l = input_line i in
    match Str.split_delim (Str.regexp "[ \t]+") l with
      "/bin/time"::"frama-c"::args
    | "frama-c"::args ->
	dump();
	let infos =
	  match Str.split_delim (Str.regexp "-cpp-extra-args") l with
	    [] | [_] -> ""
	  | _::extras ->
	      let extras =
		List.map (String.split_on_char '"') extras in
	      let extras = List.map (fun x -> List.nth x 1) extras in
	      (String.concat " " extras) ^ " " in
	curfile :=
	  infos ^
	  List.find
	    (fun x ->
	      let len = String.length x in
	      len > 2 &&
	      String.get x (len - 2) = '.' &&
	      List.mem (String.get x (len - 1)) ['c';'h'])
	    args;
	loop()
    | "[wp]"::"Proved"::"goals:"::rest ->
	let rest = String.concat " " rest in
	let rest = String.trim rest in
	(match String.split_on_char ' ' rest with
	  [c;"/";w] -> completed := int_of_string c; wanted := int_of_string w; loop()
	| _ -> failwith "bad wp line")
    | "Prover"::name::rest when List.mem "returns" rest ->
	let rest =
	  let rec loop = function
	      [] -> failwith "not possible"
	    | "returns"::rest -> rest
	    | _ :: rest -> loop rest in
	  loop rest in
	let ptimes =
	  List.fold_left
	    (fun prev cur ->
	      match get_time cur with
		Some t -> t +. prev
	      | None -> prev)
	    0. rest in
	(if ptimes > 0. && !prove <> "false."
	(* time not always provided *)
	then times := (ptimes,!goal) :: !times);
	loop()
    | "Goal"::rest -> goal := String.concat " " rest; loop()
    | "Prove:"::rest -> prove := String.concat " " rest; loop()
    | _::"Qed:"::n::_ ->
	qed := int_of_string n;
	loop()
    | _::"Alt-Ergo"::_::n::_ ->
	altergo := int_of_string n;
	loop()
    | _::"CVC4"::_::n::_ ->
	cvc4 := int_of_string n;
	loop()
    | _::"CVC5"::_::n::_ ->
	cvc5 := int_of_string n;
	loop()
    | _::"Z3"::_::n::_ ->
	z3 := int_of_string n;
	loop()
    | _ ->
	(match Str.split (Str.regexp "elapsed") l with
	  [time;_] ->
	    let tm = List.hd (List.rev (String.split_on_char ' ' time)) in
	    etime := tm
	| _ -> ());
	loop() in
  try loop() with End_of_file -> (dump(); List.rev !res)

let _ =
  let res = collect stdin in
  List.iter
    (function (file,completed,wanted,(time,goal),elapsed_time,qed,altergo,cvc4,cvc5,z3) ->
      (if completed < wanted
      then Printf.printf "=============> ");
      Printf.printf "%s:%d/%d, max %fs for %s, total time: %s\n" file completed wanted time goal elapsed_time ;
      let sum = float_of_int(qed + altergo + cvc4 + cvc5 + z3) in
      let pct x = (float_of_int (x * 100)) /. sum in
      Printf.printf "  qed: %0.2f%%, altergo: %0.2f%%, cvc4: %0.2f%%, cvc5: %0.2f%%, z3: %0.2f%%\n"
	(pct qed) (pct altergo) (pct cvc4) (pct cvc5) (pct z3))
    res
