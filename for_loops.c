#ifndef FORLOOPS
#define FORLOOPS
#include "globals.c"

typedef struct cpumask *cpumask_var_t;

#define cpumask_bits(maskp) ((maskp)->bits)

// ----------------------------------------------------------------------

/*@
requires size <= SIZE;
requires \valid_read(addr + (0 .. size-1));
assigns \nothing;

behavior above:
  assumes \exists integer j; offset <= j < size && addr[j];
  ensures offset <= \result < size;
  ensures addr[\result];
  ensures \forall integer j; offset <= j < \result ==> !addr[j];
  ensures \result < size;

behavior nowhere:
  assumes \forall integer j; offset <= j < size ==> !addr[j];
  ensures \result == size;

complete behaviors;
disjoint behaviors;
*/
static int find_next_bit(const bool *addr, unsigned long size, unsigned long offset) {
	unsigned long i;
	/*@
	loop invariant offset <= i;
	loop invariant \forall integer j;
		offset <= j < i ==> !addr[j];
	loop assigns i;
	loop variant size-i;
	*/
	for (i=offset; i<size; i++)
		if (addr[i])
			return i;
	return size;
}

#define for_each_set_bit(bit, addr, size) \
	for ((bit) = 0; (bit) = find_next_bit((addr), (size), (bit)), (bit) < (size); (bit)++)

#define for_each_cpu(cpu, mask)				\
	for_each_set_bit(cpu, cpumask_bits(mask), small_cpumask_bits)

// ----------------------------------------------------------------------

/*@
requires size <= SIZE;
requires \valid_read(addr1 + (0 .. size-1));
requires \valid_read(addr2 + (0 .. size-1));
assigns \nothing;

behavior above:
  assumes \exists integer j; offset <= j < size && addr1[j] && addr2[j];
  ensures offset <= \result < size;
  ensures addr1[\result] && addr2[\result];
  ensures \forall integer j; offset <= j < \result ==> !addr1[j] || !addr2[j];
  ensures \result < size;

behavior nowhere:
  assumes \forall integer j; offset <= j < size ==> !addr1[j] || !addr2[j];
  ensures \result == size;

complete behaviors;
disjoint behaviors;
*/
int find_next_and_bit(const bool *addr1, const bool *addr2, unsigned long size, unsigned long offset) {
	unsigned long i;
	/*@
	loop invariant offset <= i;
	loop invariant \forall integer j;
		offset <= j < i ==> !addr1[j] || !addr2[j];
	loop assigns i;
	loop variant size-i;
	*/
	for (i=offset; i<size; i++)
		if (addr1[i] && addr2[i])
			return i;
	return size;
}

#define for_each_and_bit(bit, addr1, addr2, size) \
	for ((bit) = 0;									\
	     (bit) = find_next_and_bit((addr1), (addr2), (size), (bit)), (bit) < (size);\
	     (bit)++)

#define cpumask_bits(maskp) ((maskp)->bits)

#define for_each_cpu_and(cpu, mask1, mask2)				\
	for_each_and_bit(cpu, cpumask_bits(mask1), cpumask_bits(mask2), small_cpumask_bits)

// ----------------------------------------------------------------------
/*@
requires start <= SIZE;
requires size  <= SIZE;
requires \valid_read(bitmap + (0 .. size-1));
requires \valid_read(bitmap + (0 .. start-1));
assigns \nothing;
*/
static inline
unsigned long __for_each_wrap(const bool *bitmap, unsigned long size,
				 unsigned long start, unsigned long n)
{
	unsigned long bit;

	/* If not wrapped around */
	if (n > start) {
		/* and have a bit, just return it. */
		bit = find_next_bit(bitmap, size, n);
		if (bit < size)
			return bit;

		/* Otherwise, wrap around and ... */
		n = 0;
	}

	/* Search the other part. */
	bit = find_next_bit(bitmap, start, n);
	return bit < start ? bit : size;
}
/*@
requires \valid_read(addr + (0 .. size-1));
requires size <= SIZE;
assigns \nothing;
behavior above:
  assumes \exists integer j; 0 <= j < size && addr[j];
  ensures 0 <= \result < size;
  ensures addr[\result];
  ensures \forall integer j; 0 <= j < \result ==> !addr[j];

behavior nowhere:
  assumes \forall integer j; 0 <= j < size ==> !addr[j];
  ensures \result == size;

complete behaviors;
disjoint behaviors;
*/
static int find_first_bit(const bool *addr, unsigned long size) {
	unsigned long i;
	/*@
	loop invariant \forall integer j; 0 <= j < i ==> !addr[j];
	loop assigns i;
	loop variant size-i;
	*/
	for (i=0; i<size; i++)
		if (addr[i])
			return i;
	return size;
}
/*@
requires offset < size <= SIZE;
requires \valid_read(addr + (0 .. size-1));
assigns \nothing;

behavior nowhere:
  assumes \forall integer j; 0 <= j < size ==> !addr[j];
  ensures \result == size;

  behavior above:
  assumes \exists integer j; offset <= j < size && addr[j];
  ensures offset <= \result < size;
  ensures addr[\result];
  ensures \forall integer j; offset <= j < \result ==> !addr[j];

  behavior below:
  assumes \exists integer j; 0 <= j < offset && addr[j];
  assumes \forall integer j; offset <= j < size ==> !addr[j];

  ensures \result < offset;
  ensures addr[\result];
  ensures \result < size;

  complete behaviors;
  disjoint behaviors;
*/
static inline
unsigned long find_next_bit_wrap(const bool *addr,
					unsigned long size, unsigned long offset)
{
	unsigned long bit = find_next_bit(addr, size, offset);

	if (bit < size)
		return bit;

	bit = find_first_bit(addr, offset);
	return bit < offset ? bit : size;
}

#define for_each_set_bit_wrap(bit, addr, size, start) \
	for ((bit) = find_next_bit_wrap((addr), (size), (start));		\
	     (bit) < (size);							\
	     (bit) = __for_each_wrap((addr), (size), (start), (bit) + 1))

#define for_each_cpu_wrap(cpu, mask, start)				\
	for_each_set_bit_wrap(cpu, cpumask_bits(mask), small_cpumask_bits, start)
#endif
