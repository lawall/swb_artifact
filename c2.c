#include <stdbool.h>
#include <limits.h>
#include "for_loops.c"
#include "masks.c"

// the code

#define relevant(i, env) \
      (0 <= i < small_cpumask_bits && env->cpus->bits[i] && \
      sched_group_mask(env->sd->groups)->bits[i])

#define all_separated \
 (\valid_read(env) && \
 valid_read_mask(env->cpus) && \
 \valid_read(env->dst_rq) && \
 \valid_read(env->sd) && \
 \valid_read(env->sd->groups))

/*@
requires context_ok;
requires all_separated;

requires 0 <= env->dst_cpu < small_cpumask_bits;

assigns \nothing;

behavior newly_idle:
  assumes env->idle == CPU_NEWLY_IDLE;
  ensures \result;

behavior not_newly_idle_with_idle:
  assumes env->idle != CPU_NEWLY_IDLE;
  assumes \exists integer i; relevant(i, env) && idle_cpu(i);
  ensures \forall integer i;
    relevant(i, env) ==> idle_cpu(i) ==>
    (\forall integer j; 0 <= j < i ==> relevant(j, env) ==> !idle_cpu(j)) ==>
    (\result <==> env->dst_cpu == i);
  ensures \forall integer i;
    \at(relevant(i, env) && idle_cpu(i) &&
    (\forall integer j; 0 <= j < i ==> relevant(j, env) ==> !idle_cpu(j)),Pre) ==>
    (\result <==> \at(env->dst_cpu == i,Pre));

behavior not_newly_idle_without_idle:
  assumes env->idle != CPU_NEWLY_IDLE;
  assumes \forall integer i; relevant(i, env) ==> !idle_cpu(i);
  ensures \result <==> group_balance_cpu(env->sd->groups) == env->dst_cpu;
  ensures \result <==> \at(group_balance_cpu(env->sd->groups) == env->dst_cpu,Pre);

complete behaviors;
disjoint behaviors;
*/
static int should_we_balance(struct lb_env *env)
{
	struct sched_group *sg = env->sd->groups;
	struct cpumask *sg_mask;
	int cpu, balance_cpu = -1;

	/*
	 * In the newly idle case, we will allow all the cpu's
	 * to do the newly idle load balance.
	 */
	if (env->idle == CPU_NEWLY_IDLE)
		return 1;

	sg_mask = sched_group_mask(sg);
	/* Try to find first idle cpu */
	/*@
	  loop invariant 0 <= cpu <= small_cpumask_bits;
          loop invariant \forall integer j; 0 <= j < cpu ==> relevant(j, env) ==> !idle_cpu(j);
	  loop assigns cpu;
	  loop variant small_cpumask_bits - cpu;
	*/
	for_each_cpu_and(cpu, sg_mask, env->cpus) {
		if (!idle_cpu(cpu))
			continue;

		balance_cpu = cpu;
		break;
	}

	if (balance_cpu == -1)
		balance_cpu = group_balance_cpu(sg);

	/*
	 * First idle cpu or the first cpu(busiest) in this sched group
	 * is eligible for doing load balancing at this and above domains.
	 */
	return balance_cpu == env->dst_cpu;
}
