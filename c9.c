//parameter: CONFIG_SCHED_SMT
#include <stdbool.h>
#include <limits.h>
#include "for_loops.c"
#include "masks.c"

// defined types and functions

enum {
	SD_BALANCE_NEWIDLE,
	SD_BALANCE_EXEC,
	SD_BALANCE_FORK,
	SD_BALANCE_WAKE,
	SD_WAKE_AFFINE,
	SD_ASYM_CPUCAPACITY,
	SD_ASYM_CPUCAPACITY_FULL,
	SD_SHARE_CPUCAPACITY,
	SD_SHARE_PKG_RESOURCES,
	SD_SERIALIZE,
	SD_ASYM_PACKING,
	SD_PREFER_SIBLING,
	SD_OVERLAP,
	SD_NUMA,
};

// the code

#define relevant(i, env) \
      (0 <= i < small_cpumask_bits && env->cpus->bits[i] && \
      group_balance_mask(env->sd->groups)->bits[i])

#define all_separated \
 (\valid_read(env) && \
 valid_read_mask(env->cpus) && \
 \valid_read(env->dst_rq) && \
 \valid_read(env->sd) && \
 \valid_read(env->sd->groups) && \
 separated_from_tmpmask(env->cpus))

/*@
requires context_ok;
requires all_separated;

requires 0 <= env->dst_cpu < small_cpumask_bits;

assigns should_we_balance_tmpmask[this_cpu].bits[0..small_cpumask_bits-1];

behavior race_condition:
  assumes !env->cpus->bits[env->dst_cpu];
  ensures !\result;

behavior newly_idle:
  assumes env->idle == CPU_NEWLY_IDLE;
  assumes env->cpus->bits[env->dst_cpu];
  ensures \result <==> !(env->dst_rq->nr_running > 0 || env->dst_rq->ttwu_pending);
  ensures \result <==> \at(!(env->dst_rq->nr_running > 0 || env->dst_rq->ttwu_pending),Pre);

behavior not_newly_idle_with_idle:
  assumes env->idle != CPU_NEWLY_IDLE;
  assumes env->cpus->bits[env->dst_cpu];
  assumes env->sd->flags & SD_SHARE_CPUCAPACITY;
  assumes \exists integer i; relevant(i, env) && idle_cpu(i);
  ensures \forall integer i;
    relevant(i, env) ==> idle_cpu(i) ==>
    (\forall integer j; 0 <= j < i ==> relevant(j, env) ==> !idle_cpu(j)) ==>
    (\result <==> env->dst_cpu == i);
  ensures \forall integer i;
    \at(relevant(i, env) && idle_cpu(i) &&
    (\forall integer j; 0 <= j < i ==> relevant(j, env) ==> !idle_cpu(j)),Pre) ==>
    (\result <==> \at(env->dst_cpu == i,Pre));

behavior not_newly_idle_with_idle_core:
  assumes env->idle != CPU_NEWLY_IDLE;
  assumes env->cpus->bits[env->dst_cpu];
  assumes !(env->sd->flags & SD_SHARE_CPUCAPACITY);
  assumes \exists integer i; relevant(i, env) && idle_core(i);
  ensures \forall integer i;
    relevant(i, env) ==> idle_core(i) ==>
    (\forall integer j; 0 <= j < i ==> relevant(j, env) ==> !idle_core(j)) ==>
    (\result <==> env->dst_cpu == i);
  ensures \forall integer i;
    \at(relevant(i, env) && idle_core(i) &&
    (\forall integer j; 0 <= j < i ==> relevant(j, env) ==> !idle_core(j)),Pre) ==>
    (\result <==> \at(env->dst_cpu == i,Pre));

behavior not_newly_idle_with_idle_cpu:
  assumes env->idle != CPU_NEWLY_IDLE;
  assumes env->cpus->bits[env->dst_cpu];
  assumes !(env->sd->flags & SD_SHARE_CPUCAPACITY);
  assumes \forall integer i; relevant(i, env) ==> !idle_core(i);
  assumes \exists integer i; relevant(i, env) && idle_cpu(i);
  ensures \forall integer i;
    relevant(i, env) ==> idle_cpu(i) ==>
    (\forall integer j; 0 <= j < i ==> relevant(j, env) ==> !idle_cpu(j)) ==>
    (\result ==> (env->dst_cpu == i || env->dst_cpu == group_balance_cpu(env->sd->groups)));
  ensures \forall integer i;
    relevant(i, env) ==> idle_cpu(i) ==>
    (\forall integer j; 0 <= j < i ==> relevant(j, env) ==> !idle_cpu(j)) ==>
    (env->dst_cpu == i ==> \result);
  ensures \forall integer i;
    \at(relevant(i, env) && idle_cpu(i) &&
    (\forall integer j; 0 <= j < i ==> relevant(j, env) ==> !idle_cpu(j)),Pre) ==>
    (\result ==> \at((env->dst_cpu == i || env->dst_cpu == group_balance_cpu(env->sd->groups)),Pre));
  ensures \forall integer i;
    \at(relevant(i, env) && idle_cpu(i) &&
    (\forall integer j; 0 <= j < i ==> relevant(j, env) ==> !idle_cpu(j)),Pre) ==>
    (\at(env->dst_cpu == i,Pre) ==> \result);

behavior not_newly_idle_without_idle:
  assumes env->idle != CPU_NEWLY_IDLE;
  assumes env->cpus->bits[env->dst_cpu];
  assumes \forall integer i; relevant(i, env) ==> !idle_cpu(i);
  ensures \result <==> group_balance_cpu(env->sd->groups) == env->dst_cpu;
  ensures \result <==> \at(group_balance_cpu(env->sd->groups) == env->dst_cpu,Pre);

complete behaviors;
disjoint behaviors;
*/
static int should_we_balance(struct lb_env *env)
{
	struct cpumask *swb_cpus = this_cpu_cpumask_var_ptr(should_we_balance_tmpmask);
	struct sched_group *sg = env->sd->groups;
	int cpu, idle_smt = -1;

	/*
	 * Ensure the balancing environment is consistent; can happen
	 * when the softirq triggers 'during' hotplug.
	 */
	if (!cpumask_test_cpu(env->dst_cpu, env->cpus))
		return 0;

	/*
	 * In the newly idle case, we will allow all the CPUs
	 * to do the newly idle load balance.
	 *
	 * However, we bail out if we already have tasks or a wakeup pending,
	 * to optimize wakeup latency.
	 */
	if (env->idle == CPU_NEWLY_IDLE) {
		if (env->dst_rq->nr_running > 0 || env->dst_rq->ttwu_pending)
			return 0;
		return 1;
	}

	cpumask_copy(swb_cpus, group_balance_mask(sg));
	/* Try to find first idle CPU */
	/*@
	  loop invariant 0 <= cpu <= small_cpumask_bits;

	  loop invariant a1: \forall integer j; 0 <= j < small_cpumask_bits ==> swb_cpus->bits[j] ==> group_balance_mask(sg)->bits[j];
	  loop invariant a2: idle_smt == -1 ==> \forall integer j; 0 <= j < small_cpumask_bits ==> group_balance_mask(sg)->bits[j] ==> swb_cpus->bits[j];
	  loop invariant a3: idle_smt != -1 ==> \forall integer j; 0 <= j < small_cpumask_bits ==> group_balance_mask(sg)->bits[j] ==> swb_cpus->bits[j] || !idle_core(j);

          loop invariant i1: env->sd->flags & SD_SHARE_CPUCAPACITY ==> idle_smt == -1;
          loop invariant i2: idle_smt == -1 ==> \forall integer j; 0 <= j < cpu ==> relevant(j, env) ==> !idle_cpu(j);
          loop invariant i3: idle_smt != -1 ==> relevant(idle_smt, env) && idle_cpu(idle_smt);
          loop invariant i4: idle_smt != -1 ==> \forall integer j; 0 <= j < idle_smt ==> relevant(j, env) ==> !idle_cpu(j);
          loop invariant i5: idle_smt != -1 ==> \forall integer j; idle_smt <= j < cpu ==> relevant(j, env) ==> !idle_core(j);

	  loop assigns cpu, idle_smt;
	  loop assigns *(swb_cpus->bits+(0..small_cpumask_bits-1));
	  loop variant small_cpumask_bits - cpu;
	*/
	for_each_cpu_and(cpu, swb_cpus, env->cpus) {
		if (!idle_cpu(cpu))
			continue;

		/*
		 * Don't balance to idle SMT in busy core right away when
		 * balancing cores, but remember the first idle SMT CPU for
		 * later consideration.  Find CPU on an idle core first.
		 */
		if (!(env->sd->flags & SD_SHARE_CPUCAPACITY) && !is_core_idle(cpu)) {
			if (idle_smt == -1)
				idle_smt = cpu;
			/*
			 * If the core is not idle, and first SMT sibling which is
			 * idle has been found, then its not needed to check other
			 * SMT siblings for idleness:
			 */
#ifdef CONFIG_SCHED_SMT
			cpumask_andnot(swb_cpus, swb_cpus, cpu_smt_mask(cpu));
#endif
			continue;
		}

		/* Are we the first idle CPU? */
		//@ assert a7: relevant(cpu, env);
		return cpu == env->dst_cpu;
	}

	if (idle_smt == env->dst_cpu)
		return true;

	/* Are we the first CPU of this group ? */
	return group_balance_cpu(sg) == env->dst_cpu;
}
