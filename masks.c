#ifndef MASKS
#define MASKS
#include "for_loops.c"

// external types and functions

#define valid_read_mask(m) (\valid_read(m) && \valid_read((m)->bits+(0..small_cpumask_bits-1)))
#define valid_mask(m) (\valid_read(m) && \valid((m)->bits+(0..small_cpumask_bits-1)))
#define separated_from_tmpmask(m) \
  (\forall integer i; 0 <= i < small_cpumask_bits ==> \separated((m)->bits+(0..small_cpumask_bits-1),should_we_balance_tmpmask[i].bits+(0..small_cpumask_bits-1)))

#define context_ok \
     (0 <= this_cpu < small_cpumask_bits == SIZE && \
     (\forall integer j; 0 <= j < small_cpumask_bits ==> valid_mask(&should_we_balance_tmpmask[j])) && \
     (\forall struct sched_group *sg; valid_read_mask(sched_group_cpus(sg)) && separated_from_tmpmask(sched_group_cpus(sg))) && \
     (\forall struct sched_group *sg; valid_read_mask(sched_group_mask(sg)) && separated_from_tmpmask(sched_group_mask(sg))) && \
     (\forall struct sched_group *sg; valid_read_mask(group_balance_mask(sg)) && separated_from_tmpmask(group_balance_mask(sg))) && \
     (\forall integer j; 0 <= j < small_cpumask_bits ==> valid_read_mask(cpu_smt_mask(j)) && separated_from_tmpmask(cpu_smt_mask(j))))

/*@
   axiomatic schedule_cpumask {
   logic bool idle_cpu(integer cpu);
   logic struct cpumask *sched_group_cpus(struct sched_group *sg);
   logic struct cpumask *sched_group_mask(struct sched_group *sg);
   logic struct cpumask *group_balance_mask(struct sched_group *sg);
   logic integer group_balance_cpu(struct sched_group *g);
   logic struct cpumask *cpu_smt_mask(integer cpu);

   axiom mask_refl:
     (\forall integer i; 0 <= i < SIZE ==> cpu_smt_mask(i)->bits[i]);
   axiom mask_symm:
     (\forall integer i; \forall integer j;
     0 <= i < SIZE ==> 0 <= j < SIZE ==>
     cpu_smt_mask(j)->bits[i] ==> cpu_smt_mask(i)->bits[j]);
   axiom mask_trans:
     (\forall integer i; \forall integer j; \forall integer k;
     0 <= i < SIZE ==> 0 <= j < SIZE ==> 0 <= k < SIZE ==>
     cpu_smt_mask(i)->bits[j] ==> cpu_smt_mask(j)->bits[k] ==> cpu_smt_mask(i)->bits[k]);

   predicate idle_core(integer cpu) =
     \forall integer i; 0 <= i < SIZE ==> cpu_smt_mask(cpu)->bits[i] ==> idle_cpu(i);
   }
*/


/*@
lemma idle_core_symmetry1:
  \forall integer i; 0 <= i < SIZE ==> idle_core(i) ==> \forall integer j; 0 <= j < SIZE ==> cpu_smt_mask(i)->bits[j] ==> idle_core(j);

lemma idle_core_symmetry2:
  \forall integer i; 0 <= i < SIZE ==> !idle_core(i) ==> \forall integer j; 0 <= j < SIZE ==> cpu_smt_mask(i)->bits[j] ==> !idle_core(j);

lemma ic:
  \forall integer j; 0 <= j < SIZE ==> idle_core(j) ==> \forall integer i; 0 <= i < SIZE ==> cpu_smt_mask(j)->bits[i] ==> idle_cpu(i);

lemma not_idle:
  \forall integer j; 0 <= j < SIZE ==> idle_core(j) ==> idle_cpu(j);
*/

/*@
requires 0 <= cpu < SIZE;
assigns \nothing;
ensures \result == cpu_smt_mask(cpu);
*/
static inline struct cpumask *cpu_smt_mask(int cpu);

/*@
assigns \nothing;
ensures \result == sched_group_cpus(sg);
*/
static inline struct cpumask *sched_group_cpus(struct sched_group *sg);

/*@
assigns \nothing;
ensures \result == sched_group_mask(sg);
*/
static inline struct cpumask *sched_group_mask(struct sched_group *sg);

/*@
assigns \nothing;
ensures \result == group_balance_mask(sg);
*/
static inline struct cpumask *group_balance_mask(struct sched_group *sg);

/*@
assigns \nothing;
ensures \result == group_balance_cpu(g);
*/
int group_balance_cpu(struct sched_group *g);

/*@
requires 0 <= cpu < SIZE;
assigns \nothing;
ensures \result <==> m->bits[cpu];
*/
bool cpumask_test_cpu(int cpu, struct cpumask *m);

/*@
requires 0 <= cpu < SIZE;
assigns \nothing;
ensures \result <==> idle_cpu(cpu);
*/
bool idle_cpu(int cpu);

/*@
requires context_ok;
requires 0 <= cpu < small_cpumask_bits;
requires idle_cpu(cpu);
assigns \nothing;
ensures \result <==> idle_core(cpu);
*/
static inline bool is_core_idle(int cpu)
{
	int sibling;

  /*@
  loop invariant 0 <= sibling <= small_cpumask_bits;
  loop invariant \forall integer j; 0 <= j < sibling ==> cpu_smt_mask(cpu)->bits[j] ==> idle_cpu(j);
  loop assigns sibling;
  loop variant small_cpumask_bits - sibling;
  */
	for_each_cpu(sibling, cpu_smt_mask(cpu)) {
		if (cpu == sibling)
			continue;

		if (!idle_cpu(sibling))
			return false;
	}
	return true;
}

/*@
requires small_cpumask_bits == SIZE;
requires valid_mask(dstp);
requires valid_read_mask(srcp);
requires \separated(dstp->bits+(0..small_cpumask_bits-1),srcp->bits+(0..small_cpumask_bits-1));
assigns *(dstp->bits+(0..small_cpumask_bits-1));
ensures \forall integer i; 0 <= i < small_cpumask_bits ==> (dstp->bits[i] <==> srcp->bits[i]);
*/
void cpumask_copy(struct cpumask *dstp, struct cpumask *srcp) {
  int i;
  /*@
  loop invariant 0 <= i <= small_cpumask_bits;
  loop invariant \forall integer j; 0 <= j < i ==> (dstp->bits[j] <==>  srcp->bits[j]);
  loop assigns i, *(dstp->bits+(0..small_cpumask_bits-1));
  loop variant small_cpumask_bits - i;
  */
  for(i = 0; i != small_cpumask_bits; i++)
    dstp->bits[i] =  srcp->bits[i];
}

#define this_cpu_cpumask_var_ptr(masks) &masks[this_cpu]

/*@
requires small_cpumask_bits == SIZE;
requires r2: dstp == (void *)0 || valid_mask(dstp);

requires r6: valid_read_mask(src1p);
requires r7: valid_read_mask(src2p);

requires r9:  dstp != src1p ==> \separated(dstp->bits+(0..small_cpumask_bits-1), src1p->bits+(0..small_cpumask_bits-1));
requires r10: dstp != src2p ==> \separated(dstp->bits+(0..small_cpumask_bits-1), src2p->bits+(0..small_cpumask_bits-1));

requires r11: \forall integer j; 0 <= j < small_cpumask_bits ==> \separated(dstp->bits+(0..small_cpumask_bits-1), cpu_smt_mask(j)->bits+(0..small_cpumask_bits-1));

assigns *(dstp->bits+(0..small_cpumask_bits-1));

ensures \forall integer j; 0 <= j < small_cpumask_bits ==> \at(!idle_cpu(j),Pre) ==> !idle_cpu(j);
ensures \forall integer j; 0 <= j < small_cpumask_bits ==> \at(!idle_core(j),Pre) ==> !idle_core(j);

ensures \forall integer j; 0 <= j < small_cpumask_bits ==> !idle_cpu(j) ==> \at(!idle_cpu(j),Pre);
ensures \forall integer j; 0 <= j < small_cpumask_bits ==> !idle_core(j) ==> \at(!idle_core(j),Pre);

behavior empty_dst:
  assumes dstp == (void *)0;
  ensures !\result;

behavior nonempty_dst1:
  assumes dstp != (void *)0;
  ensures \forall integer i; 0 <= i < small_cpumask_bits ==> (dstp->bits[i] == \at(src1p->bits[i] && !src2p->bits[i],Pre));
  ensures \forall integer i; 0 <= i < small_cpumask_bits ==> dstp->bits[i] ==> \at(src1p->bits[i],Pre);
  ensures \result;

complete behaviors;
disjoint behaviors;
*/
static inline bool cpumask_andnot(struct cpumask *dstp, struct cpumask *src1p, struct cpumask *src2p) {
  int i;
  if (!dstp)
    return false;
  /*@
  loop invariant 0 <= i <= small_cpumask_bits;
  loop invariant \forall integer j; 0 <= j < i ==> (dstp->bits[j] <==> \at(src1p->bits[j] && !src2p->bits[j],Pre));
  loop invariant \forall integer j; i <= j < small_cpumask_bits ==> (dstp->bits[j] <==> \at(dstp->bits[j],Pre));
  loop assigns i;
  loop assigns *(dstp->bits+(0..small_cpumask_bits-1));
  loop variant small_cpumask_bits - i;
  */
  for (i = 0; i != small_cpumask_bits; i++) {
    dstp->bits[i] = src1p->bits[i] && !src2p->bits[i];
  }
  return true;
}
#endif
