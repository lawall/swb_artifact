# swb_artifact

Frama-C and alt-ergo can be obtained using opam.

cvc4 and z3 can be obtained using apt.

Information about installing cvc5 is available here: https://cvc5.github.io/downloads.html

The main make file targets are all, all-print, smoke, smoke-print, to run the examples without and with smoke tests, respectively.
