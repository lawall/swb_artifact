#include <stdbool.h>
#include <limits.h>
#include "for_loops.c"
#include "masks.c"

// the code

#define relevant(i, env) \
      (0 <= i < small_cpumask_bits && env->cpus->bits[i] && \
      group_balance_mask(env->sd->groups)->bits[i])

#define all_separated \
 (\valid_read(env) && \
 valid_read_mask(env->cpus) && \
 \valid_read(env->dst_rq) && \
 \valid_read(env->sd) && \
 \valid_read(env->sd->groups))

/*@
requires context_ok;
requires all_separated;

requires 0 <= env->dst_cpu < small_cpumask_bits;

assigns \nothing;

behavior race_condition:
  assumes !env->cpus->bits[env->dst_cpu];
  ensures !\result;

behavior newly_idle:
  assumes env->idle == CPU_NEWLY_IDLE;
  assumes env->cpus->bits[env->dst_cpu];
  ensures \result <==> !(env->dst_rq->nr_running > 0 || env->dst_rq->ttwu_pending);

behavior not_newly_idle_with_idle:
  assumes env->idle != CPU_NEWLY_IDLE;
  assumes env->cpus->bits[env->dst_cpu];
  assumes \exists integer i; relevant(i, env) && idle_cpu(i);
  ensures \forall integer i;
    relevant(i, env) ==> idle_cpu(i) ==>
    (\forall integer j; 0 <= j < i ==> relevant(j, env) ==> !idle_cpu(j)) ==>
    (\result <==> env->dst_cpu == i);
  ensures \forall integer i;
    \at(relevant(i, env) && idle_cpu(i) &&
    (\forall integer j; 0 <= j < i ==> relevant(j, env) ==> !idle_cpu(j)),Pre) ==>
    (\result <==> \at(env->dst_cpu == i,Pre));

behavior not_newly_idle_without_idle:
  assumes env->idle != CPU_NEWLY_IDLE;
  assumes env->cpus->bits[env->dst_cpu];
  assumes \forall integer i; relevant(i, env) ==> !idle_cpu(i);
  ensures \result <==> group_balance_cpu(env->sd->groups) == env->dst_cpu;
  ensures \result <==> \at(group_balance_cpu(env->sd->groups) == env->dst_cpu,Pre);

complete behaviors;
disjoint behaviors;
*/
static int should_we_balance(struct lb_env *env)
{
	struct sched_group *sg = env->sd->groups;
	int cpu;

	/*
	 * Ensure the balancing environment is consistent; can happen
	 * when the softirq triggers 'during' hotplug.
	 */
	if (!cpumask_test_cpu(env->dst_cpu, env->cpus))
		return 0;

	/*
	 * In the newly idle case, we will allow all the CPUs
	 * to do the newly idle load balance.
	 *
	 * However, we bail out if we already have tasks or a wakeup pending,
	 * to optimize wakeup latency.
	 */
	if (env->idle == CPU_NEWLY_IDLE) {
		if (env->dst_rq->nr_running > 0 || env->dst_rq->ttwu_pending)
			return 0;
		return 1;
	}

	/* Try to find first idle CPU */
	/*@
	  loop invariant 0 <= cpu <= small_cpumask_bits;
          loop invariant \forall integer j; 0 <= j < cpu ==> relevant(j, env) ==> !idle_cpu(j);
	  loop assigns cpu;
	  loop variant small_cpumask_bits - cpu;
	*/
	for_each_cpu_and(cpu, group_balance_mask(sg), env->cpus) {
		if (!idle_cpu(cpu))
			continue;

		/* Are we the first idle CPU? */
		return cpu == env->dst_cpu;
	}

	/* Are we the first CPU of this group ? */
	return group_balance_cpu(sg) == env->dst_cpu;
}
