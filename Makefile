TIMEOUT ?= 600
RAW_ARGS=-wp -wp-rte -wp-prover=alt-ergo,cvc4,cvc5,z3 -wp-timeout $(TIMEOUT)
ARGS=${RAW_ARGS} -wp-no-warn-memory-model -kernel-warn-key annot:missing-spec=inactive
ARGSP=${ARGS} -wp-print
SMOKEARGS=${ARGS} -wp-smoke-tests
SMOKEARGSP=${ARGSP} -wp-smoke-tests
TIME=
TIME=/bin/time

check_output: check_output.ml
	ocamlc str.cma -I +str -o check_output check_output.ml

all: check_output c0 c1 c2 c3 c4 c5 c6 c7 c8 c9u c9d c10u c10d c10au c10ad c11u c11d

all-print: c0-print c1-print c2-print c3-print c4-print c5-print c6-print c7-print c8-print c9u-print c9d-print c10u-print c10d-print c10au-print c10ad-print c11u-print c11d-print

smoke: c0-smoke c1-smoke c2-smoke c3-smoke c4-smoke c5-smoke c6-smoke c7-smoke c8-smoke c9u-smoke c9d-smoke c10u-smoke c10d-smoke c10au-smoke c10ad-smoke c11u-smoke c11d-smoke

smoke-print: c0-smoke-print c1-smoke-print c2-smoke-print c3-smoke-print c4-smoke-print c5-smoke-print c6-smoke-print c7-smoke-print c8-smoke-print c9u-smoke-print c9d-smoke-print \
c10u-smoke-print c10d-smoke-print c10au-smoke-print c10ad-smoke-print c11u-smoke-print c11d-smoke-print

for_loops: for_loops.c
	${TIME} frama-c ${ARGS} for_loops.c

for_loops-print: for_loops.c
	${TIME} frama-c ${ARGS} for_loops.c

for_loops-gui: for_loops.c
	${TIME} frama-c-gui ${ARGS} for_loops.c

for_loops-smoke: for_loops.c
	${TIME} frama-c ${SMOKEARGS} for_loops.c

for_loops-smoke-print: for_loops.c
	${TIME} frama-c ${SMOKEARGSP} for_loops.c

for_loops-smoke-gui: for_loops.c
	${TIME} frama-c-gui ${SMOKEARGS} for_loops.c


masks: masks.c
	${TIME} frama-c ${ARGS} masks.c

masks-print: masks.c
	${TIME} frama-c ${ARGSP} masks.c

masks-gui: masks.c
	${TIME} frama-c-gui ${ARGS} masks.c

masks-smoke: masks.c
	${TIME} frama-c ${SMOKEARGS} masks.c

masks-smoke-print: masks.c
	${TIME} frama-c ${SMOKEARGSP} masks.c

masks-smoke-gui: masks.c
	${TIME} frama-c-gui ${SMOKEARGS} masks.c


c0: c0.c
	${TIME} frama-c ${ARGS} c0.c

c0-print: c0.c
	${TIME} frama-c ${ARGSP} c0.c

c0-gui: c0.c
	${TIME} frama-c-gui ${ARGS} c0.c

c0-smoke: c0.c
	${TIME} frama-c ${SMOKEARGS} c0.c

c0-smoke-print: c0.c
	${TIME} frama-c ${SMOKEARGSP} c0.c

c0-smoke-gui: c0.c
	${TIME} frama-c-gui ${SMOKEARGS} c0.c


c1: c1.c
	${TIME} frama-c ${ARGS} c1.c

c1-print: c1.c
	${TIME} frama-c ${ARGSP} c1.c

c1-gui: c1.c
	${TIME} frama-c-gui ${ARGS} c1.c

c1-smoke: c1.c
	${TIME} frama-c ${SMOKEARGS} c1.c

c1-smoke-print: c1.c
	${TIME} frama-c ${SMOKEARGSP} c1.c

c1-smoke-gui: c1.c
	${TIME} frama-c-gui ${SMOKEARGS} c1.c


c2: c2.c
	${TIME} frama-c ${ARGS} c2.c

c2-print: c2.c
	${TIME} frama-c ${ARGSP} c2.c

c2-gui: c2.c
	${TIME} frama-c-gui ${ARGS} c2.c

c2-smoke: c2.c
	${TIME} frama-c ${SMOKEARGS} c2.c

c2-smoke-print: c2.c
	${TIME} frama-c ${SMOKEARGSP} c2.c

c2-smoke-gui: c2.c
	${TIME} frama-c-gui ${SMOKEARGS} c2.c


c3: c3.c
	${TIME} frama-c ${ARGS} c3.c

c3-print: c3.c
	${TIME} frama-c ${ARGSP} c3.c

c3-gui: c3.c
	${TIME} frama-c-gui ${ARGS} c3.c

c3-smoke: c3.c
	${TIME} frama-c ${SMOKEARGS} c3.c

c3-smoke-print: c3.c
	${TIME} frama-c ${SMOKEARGSP} c3.c

c3-smoke-gui: c3.c
	${TIME} frama-c-gui ${SMOKEARGS} c3.c


c4: c4.c
	${TIME} frama-c ${ARGS} c4.c

c4-print: c4.c
	${TIME} frama-c ${ARGSP} c4.c

c4-gui: c4.c
	${TIME} frama-c-gui ${ARGS} c4.c

c4-smoke: c4.c
	${TIME} frama-c ${SMOKEARGS} c4.c

c4-smoke-print: c4.c
	${TIME} frama-c ${SMOKEARGSP} c4.c

c4-smoke-gui: c4.c
	${TIME} frama-c-gui ${SMOKEARGS} c4.c


c5: c5.c
	${TIME} frama-c ${ARGS} c5.c

c5-print: c5.c
	${TIME} frama-c ${ARGSP} c5.c

c5-gui: c5.c
	${TIME} frama-c-gui ${ARGS} c5.c

c5-smoke: c5.c
	${TIME} frama-c ${SMOKEARGS} c5.c

c5-smoke-print: c5.c
	${TIME} frama-c ${SMOKEARGSP} c5.c

c5-smoke-gui: c5.c
	${TIME} frama-c-gui ${SMOKEARGS} c5.c


c6: c6.c
	${TIME} frama-c ${ARGS} c6.c

c6-print: c6.c
	${TIME} frama-c ${ARGSP} c6.c

c6-gui: c6.c
	${TIME} frama-c-gui ${ARGS} c6.c

c6-smoke: c6.c
	${TIME} frama-c ${SMOKEARGS} c6.c

c6-smoke-print: c6.c
	${TIME} frama-c ${SMOKEARGSP} c6.c

c6-smoke-gui: c6.c
	${TIME} frama-c-gui ${SMOKEARGS} c6.c


c7: c7.c
	${TIME} frama-c ${ARGS} c7.c

c7-print: c7.c
	${TIME} frama-c ${ARGSP} c7.c

c7-gui: c7.c
	${TIME} frama-c-gui ${ARGS} c7.c

c7-smoke: c7.c
	${TIME} frama-c ${SMOKEARGS} c7.c

c7-smoke-print: c7.c
	${TIME} frama-c ${SMOKEARGSP} c7.c

c7-smoke-gui: c7.c
	${TIME} frama-c-gui ${SMOKEARGS} c7.c


c8: c8.c
	${TIME} frama-c ${ARGS} c8.c

c8-print: c8.c
	${TIME} frama-c ${ARGSP} c8.c

c8-gui: c8.c
	${TIME} frama-c-gui ${ARGS} c8.c

c8-smoke: c8.c
	${TIME} frama-c ${SMOKEARGS} c8.c

c8-smoke-print: c8.c
	${TIME} frama-c ${SMOKEARGSP} c8.c

c8-smoke-gui: c8.c
	${TIME} frama-c-gui ${SMOKEARGS} c8.c


c9u: c9.c
	${TIME} frama-c ${ARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c9.c

c9u-print: c9.c
	${TIME} frama-c ${ARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" -wp-print c9.c

c9u-gui: c9.c
	${TIME} frama-c-gui ${ARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c9.c

c9u-smoke: c9.c
	${TIME} frama-c ${SMOKEARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c9.c

c9u-smoke-print: c9.c
	${TIME} frama-c ${SMOKEARGSP} -cpp-extra-args="-U CONFIG_SCHED_SMT" c9.c

c9u-smoke-gui: c9.c
	${TIME} frama-c-gui ${SMOKEARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c9.c


c9d: c9.c
	${TIME} frama-c ${ARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c9.c

c9d-print: c9.c
	${TIME} frama-c ${ARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" -wp-print c9.c

c9d-gui: c9.c
	${TIME} frama-c-gui ${ARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c9.c

c9d-smoke: c9.c
	${TIME} frama-c ${SMOKEARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c9.c

c9d-smoke-print: c9.c
	${TIME} frama-c ${SMOKEARGSP} -cpp-extra-args="-D CONFIG_SCHED_SMT" c9.c

c9d-smoke-gui: c9.c
	${TIME} frama-c-gui ${SMOKEARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c9.c


c10u:
	${TIME} frama-c ${ARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c10.c

c10u-print:
	${TIME} frama-c ${ARGSP} -cpp-extra-args="-U CONFIG_SCHED_SMT" c10.c

c10u-gui:
	${TIME} frama-c-gui ${ARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c10.c

c10u-smoke:
	${TIME} frama-c ${SMOKEARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c10.c

c10u-smoke-print:
	${TIME} frama-c ${SMOKEARGSP} -cpp-extra-args="-U CONFIG_SCHED_SMT" c10.c

c10u-smoke-gui:
	${TIME} frama-c-gui ${SMOKEARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c10.c


c10d:
	${TIME} frama-c ${ARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c10.c

c10d-print:
	${TIME} frama-c ${ARGSP} -cpp-extra-args="-D CONFIG_SCHED_SMT" c10.c

c10d-gui:
	${TIME} frama-c-gui ${ARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c10.c

c10d-smoke:
	${TIME} frama-c ${SMOKEARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c10.c

c10d-smoke-print:
	${TIME} frama-c ${SMOKEARGSP} -cpp-extra-args="-D CONFIG_SCHED_SMT" c10.c

c10d-smoke-gui:
	${TIME} frama-c-gui ${SMOKEARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c10.c


c10au:
	${TIME} frama-c ${ARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c10a.c

c10au-gui:
	${TIME} frama-c-gui ${ARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c10a.c

c10au-print:
	${TIME} frama-c ${ARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" -wp-print c10a.c

c10au-smoke:
	${TIME} frama-c ${SMOKEARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c10a.c

c10au-smoke-gui:
	${TIME} frama-c-gui ${SMOKEARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c10a.c

c10au-smoke-print:
	${TIME} frama-c ${SMOKEARGSP} -cpp-extra-args="-U CONFIG_SCHED_SMT" c10a.c


c10ad:
	${TIME} frama-c ${ARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c10a.c

c10ad-gui:
	${TIME} frama-c-gui ${ARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c10a.c

c10ad-print:
	${TIME} frama-c ${ARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" -wp-print c10a.c

c10ad-smoke:
	${TIME} frama-c ${SMOKEARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c10a.c

c10ad-smoke-gui:
	${TIME} frama-c-gui ${SMOKEARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c10a.c

c10ad-smoke-print:
	${TIME} frama-c ${SMOKEARGSP} -cpp-extra-args="-D CONFIG_SCHED_SMT" c10a.c


c11u:
	${TIME} frama-c ${ARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c11.c

c11u-print:
	${TIME} frama-c ${ARGSP} -cpp-extra-args="-U CONFIG_SCHED_SMT" c11.c

c11u-gui:
	${TIME} frama-c-gui ${ARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c11.c

c11u-smoke:
	${TIME} frama-c ${SMOKEARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c11.c

c11u-smoke-print:
	${TIME} frama-c ${SMOKEARGSP} -cpp-extra-args="-U CONFIG_SCHED_SMT" c11.c

c11u-smoke-gui:
	${TIME} frama-c-gui ${SMOKEARGS} -cpp-extra-args="-U CONFIG_SCHED_SMT" c11.c




c11d:
	${TIME} frama-c ${ARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c11.c

c11d-print:
	${TIME} frama-c ${ARGSP} -cpp-extra-args="-D CONFIG_SCHED_SMT" c11.c

c11d-gui:
	${TIME} frama-c-gui ${ARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c11.c

c11d-smoke:
	${TIME} frama-c ${SMOKEARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c11.c

c11d-smoke-print:
	${TIME} frama-c ${SMOKEARGSP} -cpp-extra-args="-D CONFIG_SCHED_SMT" c11.c

c11d-smoke-gui:
	${TIME} frama-c-gui ${SMOKEARGS} -cpp-extra-args="-D CONFIG_SCHED_SMT" c11.c
