#!/bin/bash

TIMEOUT=600

while getopts "t:" opt; do
  case $opt in
    t)
      TIMEOUT=$OPTARG
      ;;
    \?)
      echo invalid option
      ;;
  esac
done

make 2>&1 > /dev/null

echo -n "c0 w/o smoke: "
make TIMEOUT=$TIMEOUT c0-print    2>&1 | ./check_output
echo -n "c1 w/o smoke: "
make TIMEOUT=$TIMEOUT c1-print    2>&1 | ./check_output
echo -n "c2 w/o smoke: "
make TIMEOUT=$TIMEOUT c2-print    2>&1 | ./check_output
echo -n "c3 w/o smoke: "
make TIMEOUT=$TIMEOUT c3-print    2>&1 | ./check_output
echo -n "c4 w/o smoke: "
make TIMEOUT=$TIMEOUT c4-print    2>&1 | ./check_output
echo -n "c5 w/o smoke: "
make TIMEOUT=$TIMEOUT c5-print    2>&1 | ./check_output
echo -n "c6 w/o smoke: "
make TIMEOUT=$TIMEOUT c6-print    2>&1 | ./check_output
echo -n "c7 w/o smoke: "
make TIMEOUT=$TIMEOUT c7-print    2>&1 | ./check_output
echo -n "c8 w/o smoke: "
make TIMEOUT=$TIMEOUT c8-print    2>&1 | ./check_output
echo -n "c9u w/o smoke: "
make TIMEOUT=$TIMEOUT c9u-print   2>&1 | ./check_output
echo -n "c9d w/o smoke: "
make TIMEOUT=$TIMEOUT c9d-print   2>&1 | ./check_output
echo -n "c10u w/o smoke: "
make TIMEOUT=$TIMEOUT c10u-print  2>&1 | ./check_output
echo -n "c10d w/o smoke: "
make TIMEOUT=$TIMEOUT c10d-print  2>&1 | ./check_output
echo -n "c10au w/o smoke: "
make TIMEOUT=$TIMEOUT c10au-print 2>&1 | ./check_output
echo -n "c10ad w/o smoke: "
make TIMEOUT=$TIMEOUT c10ad-print 2>&1 | ./check_output
echo -n "c11u w/o smoke: "
make TIMEOUT=$TIMEOUT c11u-print  2>&1 | ./check_output
echo -n "c11d w/o smoke: "
make TIMEOUT=$TIMEOUT c11d-print  2>&1 | ./check_output

echo -n "c0 w/ smoke: "
make TIMEOUT=$TIMEOUT c0-smoke-print    2>&1 | ./check_output
echo -n "c1 w/ smoke: "
make TIMEOUT=$TIMEOUT c1-smoke-print    2>&1 | ./check_output
echo -n "c2 w/ smoke: "
make TIMEOUT=$TIMEOUT c2-smoke-print    2>&1 | ./check_output
echo -n "c3 w/ smoke: "
make TIMEOUT=$TIMEOUT c3-smoke-print    2>&1 | ./check_output
echo -n "c4 w/ smoke: "
make TIMEOUT=$TIMEOUT c4-smoke-print    2>&1 | ./check_output
echo -n "c5 w/ smoke: "
make TIMEOUT=$TIMEOUT c5-smoke-print    2>&1 | ./check_output
echo -n "c6 w/ smoke: "
make TIMEOUT=$TIMEOUT c6-smoke-print    2>&1 | ./check_output
echo -n "c7 w/ smoke: "
make TIMEOUT=$TIMEOUT c7-smoke-print    2>&1 | ./check_output
echo -n "c8 w/ smoke: "
make TIMEOUT=$TIMEOUT c8-smoke-print    2>&1 | ./check_output
echo -n "c9u w/ smoke: "
make TIMEOUT=$TIMEOUT c9u-smoke-print   2>&1 | ./check_output
echo -n "c9d w/ smoke: "
make TIMEOUT=$TIMEOUT c9d-smoke-print   2>&1 | ./check_output
echo -n "c10u w/ smoke: "
make TIMEOUT=$TIMEOUT c10u-smoke-print  2>&1 | ./check_output
echo -n "c10d w/ smoke: "
make TIMEOUT=$TIMEOUT c10d-smoke-print  2>&1 | ./check_output
echo -n "c10au w/ smoke: "
make TIMEOUT=$TIMEOUT c10au-smoke-print 2>&1 | ./check_output
echo -n "c10ad w/ smoke: "
make TIMEOUT=$TIMEOUT c10ad-smoke-print 2>&1 | ./check_output
echo -n "c11u w/ smoke: "
make TIMEOUT=$TIMEOUT c11u-smoke-print  2>&1 | ./check_output
echo -n "c11d w/ smoke: "
make TIMEOUT=$TIMEOUT c11d-smoke-print  2>&1 | ./check_output
